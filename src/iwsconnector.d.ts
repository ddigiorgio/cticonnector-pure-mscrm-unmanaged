type IwsLayoutType = "widget" | "frame" | "";
type IwsIntegrationType = "" | "wde" | "wwe" | "pure-embeddable" | "pure-integration";
type IwsLayoutPosition = "left" | "right";
type IwsLayoutSize = "small" | "medium";
type CtiService = "PureCloud" | "WDE";
interface CtiMessage {
    EVENT: string;
    CallType: string;
    MediaType: mediaType;
    Service: CtiService;
    AgentID: string;//agent.id
    ANI: string;//customer.address
    CustomerID: string;//customer.id,
    DNIS: string;//agent.address
    InteractionID: string;
    ConnectionID: string;
    ConnectedTime: string;
    DisconnectedTime: string;
    attachdata: any;
    EmailAddress: string;
    Duration: number;
    TimeStamp: number;
    SourceMsg:any;
  }
interface String {
    format(...args);
    startsWith(value: string);
}
interface IwsLayoutParams {
    context: Window | HTMLIFrameElement;
    placeHolder?: string;
    layoutType?: IwsLayoutType;
    integrationType: IwsIntegrationType;
    url?: string;
    width?: string;
    height?: string;
    position?: IwsLayoutPosition;
    draggable?: boolean;
    sideMove?: boolean;
    size?: IwsLayoutSize;
    auth?: PureAuth;
    activationCallback?: Function;
    messageCallback?:Function;
    pureClient?:PureClient;
}


interface IwsCore {

    getLayoutParams(): IwsLayoutParams;

    initCTI(param: IwsLayoutParams): void;

    enableCTI(): void;

    hideCTIPanel(): void;

    openCTIPanel(): void;

    closeCTIPanel();

    getSessionId();

    setIwsApplicationName(iwsappname);

    setIwsInitData(iwsInitData);

    getIwsApplicationName();

    getVersion();

    setServerUrl(server, port);

    setSecureServerUrl(server, port);

    setProtocol(myparams);

    isWebSocket();

    isHttpProtocol();

    //createSecureConnection(server, port, myparams);

    createConnection(server, port, myparams);

    //createConnectionRecovery();

    getWdeJsonp();

    //testJsonp();

    //startHttpConnection(myparams);

    //startWebSocketConnection(myparams);

    //startConnection(myparams);

    //getEvent();

    handleEvent(message);

    //handleEventMultyInteractions(sevent, message);

    isSelectedInteraction(message);

    callSafetyFunction(function_name, message);

    checkFunction(function_name);

    //onKeepAlive(message);

    getMessageId(message);


    //log_debug(message);

    //log_info(message);

    //log_warn(message);

    //log_error(message);

    //log_infoFormat();

    //log_debugFormat();

    //log_warnFormat();

    //log_errorFormat();

    //createFormatMessage(arguments);

    //trace(message);

    updateJSONObjectInMemory(message);

    removeJSONObjectInMemory(key);

    addJSONObjectInMemory(message, selected?);

    addJSONObjectInMemoryCampaign(message, selected?);

    //addInteraction(key, message);

    getInteraction(key);

    existInteraction(key);

    removeInteraction(key);

    //getFirstInteraction();

    //getFirstInteractionId();

    existActiveInteraction(medianame);

    countActiveInteractions(medianame);

    countInteractions();

    //isValidProperty(object);

    //isCompactMenu();

    //getElementFromCombo(key);

    //removeInteractionOption(key);

    //addInteractionOption(value, text, selected);

    //getUIInteractionCounter();

    //updateUIInteractionCounter();

    selectInteractionOptionByMessage(message);

    changeInteractionText(message, messageFormat, parametersValue);

    updateInteractionText(message);

    //selectInteractionOption(key);

    hasSelectedInteraction();

    addEmptyOption();

    removeEmptyOption();

    removeDefaultOption();

    //addDefaultOption();

    //switchInteraction();

    //notExistInteractionOptions();

    //existOptionInteractions();

    getSelectedInteractionId();

    getSelectedInteraction();

    //getComboInteraction();

    isEnablePlaceHolderInteraction(message);

    showConnectedState();

    showActivedState();

    showDisconnectedState();

    //changeImage(colorname);

    isActiveConnection();

    isConnected();

    setSipDisasterRecovery(val);

    isSipDisasterRecovery();

    //setMultyInteractions(val);

    //isMultyInteractions();

    //setMultyInteractionsEvents(val);

    //isMultyInteractionsEvents();

    setPlaceHolder(val);

    isEnablePlaceHolder();

    showEventMessage(text, millisectime, labelcolor?);

    //getElement(id);

    setEnableRightClick(benable);

    disableRightClick();

    initIWSToolBar(placeframe, divname, imagepath, prefiximage);

    //handleMenuClick();

    //handleOptionClick(_ele);

    //enableCompactMenu();

    showIWSToolBar(cmbInteraction, imgState, lblMessage);

    //getInternetExplorerVersion();

    //isIE8orLater();

    addFormatInteraction(media_name, message_format, parameters_value);

    getFormatInteraction(media_name);

    getMultiInteractionFormated(media_name, message);

    isCampaign(message);

    getRecordHandle(message);

    getCampaignName(message);

    addFilter(eventname, action);

    isFilter();

    getActionFilter(message);

    getActionFilterByName(eventname);

    executeActionFilter(message, action);

    /*        
            Hashtable();
    
            hashtable_clear();
    
            hashtable_containsKey(key);
    
            hashtable_containsValue(value);
    
            hashtable_get(key);
    
            hashtable_isEmpty();
    
            hashtable_keys();
    
            hashtable_put(key, value);
    
            hashtable_remove(key);
    
            hashtable_size();
    
            hashtable_toString();
    
            hashtable_toJson();
    
            hashtable_toJsonObject();
    
            hashtable_values();
    
            */
    createUserData();

    isAppMode();

    setAppMode();

    receivePostMessage(event);

}

interface IwsCommand {
    Ready();

    /**
     * Call the Voice NotReady Command on IWS
     * @param actioncode: string
     */
    NotReady(actioncode);

    /**
     * Call the Ready Command on all IWS channels
     */
    ReadyAll();

    /**
     * Call the NotReady Command on all IWS channel
     * @param actioncode: string
     */
    NotReadyAll(actioncode);

    /**
     * Call the NotReady Command on IWS media channel
     * @param medianame
     * @return
     */
    MediaReady(medianame);

    /**
     * Call the NotReady Command on IWS media channel
     * @param medianame
     * @param actioncode
     * @return
     */
    MediaNotReady(medianame, actioncode);

    /**
     * Call the Ready Command on all IWS channels
     */
    ACWAll();

    /**
     * Call the ACW Command on IWS media channel
     * @param medianame
     * @return
     */
    MediaACW(medianame);

    /**
     * Execute Agent Logout
     */
    Logout();

    /**
     * Execute Agent Login
     */
    Login();

    /**
     * Execute IWS Close
     */
    CloseIWS();

    /**
     * Make a Voice Call on IWS
     * @param number: number to call
     * @param params: custom "UserData" collection (as showed in the example)
     *  * Example: 
     * 	var mycollection = createUserData();
     * 	mycollection.put("ACTIVITY_ID", activityId);
     * 	MakeCall("89899", mycollection);
     * or
     * 	MakeCall("89899");
     */
    MakeCall(number, params);

    /**
     * It is different from the MakeCall function, because accept the collection attachdata received by an event.
     * @param number
     * @param params
     * @return
     * Example
     * MakeCallEx(message.attachdata.GSW_PHONE, message.attachdata);
     */
    MakeCallEx(number, params);

    /**
     * Make a voice call on IWS related a Campaign record
     * @param number
     * @param recordhandle
     * @return
     * Example
     * var recordhandle = getRecordHandle(message);
     * MakeCallCampaign(number, recordhandle);
     */
    MakeCallCampaign(number, recordhandle);

    /**
     * Obsolete function
     * Set the attach-data on the current Interaction 
     * Example: setattachdata("'param1' : 'value1', 'param2' : 'value2'");
     * @param params: string
     */
    setattachdata(params);

    /**
     * Set the attach-data on the Interaction related the "interactionid"
     * Example: 
     * 	var mycollection = createUserData();
     * 	mycollection.put("ACTIVITY_ID", activityId);
     * 	SetAttachdataById(interactionid, mycollection);
     * 
     * @param interactionid: interaction Id
     * @param params: Attach-Data in JSON format
     */
    SetAttachdataById(interactionid, params);


    /**
     * Set the attach-data on the Interaction related the "interactionid"
     * Example: 
     * 	var mycollection = createUserData();
     * 	mycollection.put("ACTIVITY_ID", activityId);
     * 	SetAttachdataByIdEx(interactionid, '1001', mycollection);
     * 
     * @param interactionid: interaction Id
     * @param thisdn: current Agnet DN
     * @param params: Attach-Data in JSON format
     */
    SetAttachdataByIdEx(interactionid, thisdn, params);

    /**
     * Set the attach-data on the Interaction related the "interactionid" and customerId
     * This method is used by the pure-integration type
     * Example: 
     * 	iwscommand.SetAttachdataByIdAndCustomerId("8564ec6e-b2ed-48dd-90da-344f62c05f33", "94427cc8-609a-4b2a-a7c5-784ad7bdd581",{"test":"valuetest"});
     * 
     * @param interactionId: interaction Id
     * @param customerId: current Agnet DN
     * @param params: Attach-Data in JSON format
     */
    SetAttachdataByIdAndCustomerId(interactionId: string, customerId: string, params: Object)

    SetAttachdataPlaceHolder(interactionid);

    /**
     * Set the attach-data on the Interaction selected in the Combo Interaction
     * Example: 
     * 	var mycollection = createUserData();
     * 	mycollection.put("ACTIVITY_ID", activityId);
     * 	setattachdataOnSelectedInteraction(mycollection);
     * 
     * @param params: Attach-Data in JSON format
     */
    SetAttachdataOnSelectedInteraction(params);

    /**
     * Set the DispositionCode
     * Example: 
     * 	SetDispositionCode(interactionid, dispositioncode);
     * 
     * @param interactionid 
     * @param dispositioncode: string that match in Genesys side
     */
    SetDispositionCode(interactionid, dispositioncode);

    /**
     * Call the GetRecord command in IWS
     * If the campaignname is empty, will be invoked the GetRecord on all Active Preview Campaign
     * @param campaignname
     */
    GetRecord(campaignname);

    /**
     * 
     * Example:
     * onEventReleasedOutbound(message)
     * {	
     * 		var customfields = createUserData();
     * 		customfields("ACTIVITY_ID", activityId);
     * 		var rh = getRecordHandle(message);
     * 		RecordProcessed(rh, 0, "", customfields);
     * 		//to change the contact_info 
     *  	RecordProcessed(rh, 0, "2005", customfields);
     * }
     * 
     * @param recordhandle 
     * @param callresult
     * @param phonenumber
     * @param customfields
     * @return
     * public enum CallResult
        {
            OK = 0,
            GENERAL_ERROR = 3,
            SYSTEM_ERROR = 4,
            BUSY = 6,
            NO_ANSWER = 7,
            SIT_DETECTED = 8,
            ANSWERING_MACHINE = 9,
            ALL_TRUNKS_BUSY = 10,
            SIT_INVALID_NUM = 11,
            SIT_VACANT = 12,
            SIT_OPERINTERCEPT = 13,
            SIT_UNKNOWN = 14,
            SIT_NO_CIRCUIT = 15,
            SIT_REORDER = 16,
            FAXDETECTED = 17,
            ABANDONED = 21,
            DROPPED = 26,
            DROPPED_NO_ANSWER = 27,
            UNKNOWN = 28,
            SILENCE = 32,
            ANSWER = 33,
            NUTONE = 34,
            NO_DIAL_TONE = 35,
            NO_PROGRESS = 36,
            NO_RINGBACK = 37,
            NO_ESTABLISHED = 38,
            PAGER_DETECTED = 39,
            WRONG_PARTY = 40,
            DIAL_ERROR = 41,
            CALL_DROP_ERROR = 42,
            SWITCH_ERROR = 43,
            NO_FREE_PORT_ERROR = 44,
            TRANSFER_ERROR = 45,
            STALE = 46,
            AGENT_CALLBACK_ERROR = 47,
            GROUP_CALLBACK_ERROR = 48,
            DO_NOT_CALL = 51,
            CANCEL_RECORD = 52,
            WRONG_NUMBER = 53,
        }
     */
    RecordProcessed(recordhandle, callresult, phonenumber, customfields);

    /**
     * Allow to Reschedule an Outbound Record
     * Example:
     * 	var customfields = createUserData();
     * 	customfields("ACTIVITY_ID", activityId);
     * 	var rh = getRecordHandle(message);
     *  //"http://www.w3schools.com/jsref/jsref_obj_date.asp"
     *  var mydate = new Date();
     * 	RecordReschedule(rh, 1, customfields, mydate.getTime());
     * 
     * @param recordhandle 
     * @param schedulemode
     * @param rescheduletime
     * @param phonenumber
     * @param customfields
     * @return
     * public enum CallBackType
        {
            Unknown = 0,
            Personal = 1,
            Campaign = 2,
        }
     */
    RecordReschedule(recordhandle, schedulemode, rescheduletime, phonenumber, customfields);

    /**
     * Reject an Outbound Record
     * @param recordhandle
     * @param customfields
     * @return
     * Example:
     * 	var rh = getRecordHandle(message);
     * 	RecordRejected(rh);
     
     */
    RecordRejected(recordhandle, customfields);

    /**
     * Reject an Outbound Record
     * @param recordhandle
     * @param customfields
     * @return
     * Example:
     * 	var rh = getRecordHandle(message);
     * 	RecordCancel(rh);
     */
    RecordCancel(recordhandle, customfields);

    /**
     * Move the Email in the WorkBin
     * Example: InteractionEmailMoveToWorkbin('0000Ra8AGWMF00C1', 'Workbin-E-Mail', 'workbin.email.in-progress')
     * @param interactionid
     * @param workbinid
     * @param workbinotionname
     */
    InteractionEmailMoveToWorkbin(interactionid, workbinid, workbinotionname, attachdata);

    /**
     * Take the Email from Workbin
     * Example: InteractionEmailActionFromWorkbinPullById('0000Ra8AGWMF00C1', 'Workbin-E-Mail', 'WorkbinsView')
     * @param interactionid
     * @param workbinid
     * @param worningmessagetarget
     */
    InteractionEmailActionFromWorkbinPullById(interactionid, workbinid, worningmessagetarget);

    /**
     * Move the WorkItem in the WorkBin
     * @param interactionid
     * @param workbinid
     * @param workbinotionname
     */
    InteractionWorkItemMoveToWorkbin(interactionid, workbinid, workbinotionname, attachdata);

    /**
     * Take the WorkItem from WorkBin
     * @param interactionid
     * @param workbinid
     * @param worningmessagetarget
     */
    InteractionWorkItemActionFromWorkbinPullById(interactionid, workbinid, worningmessagetarget);

    /**
     * Take the OpenMedia Interaction from WorkBin
     * @param interactionid
     * @param workbinid
     * @param worningmessagetarget
     */
    InteractionOpenMediaPullFromWorkbinById(interactionid, workbinid, worningmessagetarget);

    /**
     * Place the OpenMedia Interaction in Queue
     * @param interactionId
     * @param destination
     * @param userdata
     */
    InteractionOpenMediaPlaceInQueue(interactionId, destination, userdata);

    /**
     * Place the OpenMedia Interaction in Queue
     * @param interactionId
     * @param destination
     * @param userdata
     * @param reason
     * @param extension
     * @param transferringAgentName
     * @param transferringReason
     * @param transferringTargetType
     * @param transferringTarget
     * Example: 
     * var mycollection = createUserData();
     * mycollection.put("ACTIVITY_ID", activityId);
     * InteractionOpenMediaPlaceInQueueEx("07UD032HCVHTJ01N","iWD_Pending", userdata, "", "","ibm","", "InteractionQueue","iWD_Pending");	
     */
    InteractionOpenMediaPlaceInQueueEx(interactionId, destination, userdata, reason, extension, transferringAgentName, transferringReason, transferringTargetType, transferringTarget);

    /**
     * Allow to mark done an Interaction.
     * The related windows will be closed
     * @param interactionId
     * @return
     */
    InteractionMarkDone(interactionId);

    /**
     * Allow to release a Voice Interaction.
     * @param interactionId
     * @return
     */
    InteractionRelease(interactionId);

    /**
     * Allow a single step transfer
     * @param interactionId
     * @param destination
     * @param attachdata
     * @return
     * Example: 
     * var attachdata = createUserData();
     * attachdata.put("param1", "123456");
     * InteractionVoiceSingleStepTransfer("07UD032HCVHTJ01N","2001", attachdata);	
     */
    InteractionVoiceSingleStepTransfer(interactionId, destination, attachdata);

    /**
     * Allow to init a double steps Transfer
     * @param interactionId
     * @param destination
     * @param attachdata
     * @return
     * Example: 
     * var id = getSelectedInteractionId();
     * InteractionVoiceInitTransfer(id,"2001", null);	
     */
    InteractionVoiceInitTransfer(interactionId, destination, attachdata);

    /**
     * Allow to complete a Transfer
     * @param interactionId
     * @return
     * Example: 
     * var id = getSelectedInteractionId();
     * InteractionVoiceCompleteTransfer(id);	
     */
    InteractionVoiceCompleteTransfer(interactionId);

    /**
     * Allow a single step Conference
     * @param interactionId
     * @param destination
     * @param attachdata
     * @return
     * Example: 
     * var id = getSelectedInteractionId();
     * InteractionVoiceSingleStepConference(id,"2001", null);	
     */
    InteractionVoiceSingleStepConference(interactionId, destination, attachdata);

    /**
     * Allow to init a Conference
     * @param interactionId
     * @param destination
     * @param attachdata
     * @return
     * Example: 
     * var id = getSelectedInteractionId();
     * InteractionVoiceInitConference(id,"2001", null);	
     */
    InteractionVoiceInitConference(interactionId, destination, attachdata);

    /**
     * Allow to complete a Conference
     * @param interactionId
     * @return
     * Example: 
     * var id = getSelectedInteractionId();
     * InteractionVoiceCompleteConference(id);	
     */
    InteractionVoiceCompleteConference(interactionId);

    /**
     * Allow to open the IWS window to manage a new Outbound Email
     * @param to
     * @param cc
     * @param bcc
     * @param subject
     * @param bodymessage
     * @param attachdata
     * Example: 
     * var attachdata = createUserData();
     * attachdata.put("param1", "123456");
     * var bodymessage = "<HTML><HEAD></HEAD><BODY><A href='http://www.softphone.it'>softphone</A></BODY></HTML>";
     * CreateNewOutboundEmail("info@softphone.it","", "", "subject test", bodymessage, attachdata);
     */
    CreateNewOutboundEmail(to, cc, bcc, subject, bodymessage, attachdata);

    /**
    * Reply an email by id (from the workbin too)
    * @param id: interactionid
    * @return
    * Example:
    * ReplyEmailById('fw43w34gwg4');
    */
    ReplyEmailById(id);

    /**
    * Send an email without Agent interaction 
    * @param to:
    * @param cc:
    * @param bcc:
    * @param subject:
    * @param bodymessage:
    * @param attachdata: 
    * @param attachments: JSON array with all files path. The path must to be a valid path on client machine.
    * @return
    * Example:
    * var bodymessage = "<HTML><HEAD></HEAD><BODY><A href='http://www.softphone.it'>softphone</A></BODY></HTML>";
    * var myattachments = [];
    * myattachments.push('C:\\app\\lock.png');
    * myattachments.push('C:\\app\\pippo.pdf');
    * var mycollection = createUserData();
    * mycollection.put("param1", "value1");
    * mycollection.put("param2", "value2");		
    * SendSilentEmail("client@laboratorio.local", null, "", "subject test", bodymessage, mycollection, myattachments);
    */
    SendSilentEmail(to, cc, bcc, subject, bodymessage, attachdata, attachments);

    /**
     * Valid just on HTTP protocol
     */
    SendSilentEmailEx(to, cc, bcc, subject, bodymessage, attachdata, attachments, successFunction, errorFunction);

    /**
    * Send an email without Agent interaction 
    * @param id: interactionid
    * @param attachments: JSON array with all files path. The path must to be a valid path on client machine.
    * @return
    * Example:
    * var myattachments = [];
    * myattachments.push('C:\\app\\lock.png');
    * myattachments.push('C:\\app\\pippo.pdf');
    * //get the current email id
    * var id = getSelectedInteractionId();	
    * AddEmailAttachments(id, myattachments);
    */
    AddEmailAttachments(id, attachments);

    /**
    * Send an active email by Id
    * @param id: interactionid
    * Example:
    * //get the current email id
    * var id = getSelectedInteractionId();
    * SendEmailById(id);
    */
    SendEmailById(id);

    /**
    * Insert the body in an active email
    * @param id: interactionid
    * @param bodymessage:
    * @return
    * Example:
    * var bodymessage = "<HTML><HEAD></HEAD><BODY><A href='http://www.softphone.it'>softphone</A></BODY></HTML>";
    * var id = getSelectedInteractionId();
    * AddEmailBody(id, bodymessage);
    */
    AddEmailBody(id, bodymessage);

    /**
     * Show a Toaster of information type on IWS 
     * @param message
     * @return
     */
    PublishInformation(message);

    PublishWarning(message);

    PublishError(message);

    PublishMessage(message);

    PublishEvent(eventname, message);

    ExecuteDelegatedCommand(delegated_id);

    RemoveDelegatedCommand(delegated_id);

    //getJsonValue(objvalue);

    //getValueIfDefined(objvalue);

    sessionexit();

    sessionexitPotentialRefresh();

    //  executeCustomCommand(message);

    executeStandardCommand(myparams);

    //executeHttpCommand(mycommand, myparams);

    //executeHttpCommandEx(mycommand, myparams, successFunction, errorFunction);

    //executeWebSocketCommand(mycommand, myparams);

    //executeCommand(mycommand, myparams);

    removeExitOnDocumentUnload();

    //callJsonpSync(myparams);

    /**
    * Notify WDE, that there is a new Interaction Selected on the CRM 
    */
    SetInteractionOnWde(interactionid);

    ActiveMe(url, cmdshow, topmost, browser);

    /**
    * Select/Switch the Interaction on WDE
    * This option is valid only if WDE is in ... mode
    */
    InteractionSwitch(interactionid);

    //============= Remote Functions ==========
    /**
    * return the Agent Channel State 
    */
    GetWDEAgentState(callbackfunction);

    /**
    * return all available Interactions
    */
    GetWDEAllInteractions(callbackfunction);

    /**
    * return the last Interaction received
    */
    GetWDESelectedInteraction(callbackfunction);

    GetWDEInteractionById(connectionid, callbackfunction);

    executeStandardMethod(myparams, callbackfunction);

    getMethod(mymethod, myparams, callbackfunction);

    //CloseTab(interactionid);

    //SendThroughPostMessage(message);
}

interface Log {
    setLogLevel(level: number);
    debug(msg: string);
    info(msg: string);
    warn(msg: string);
    error(msg: string);
    infoFormat(...args);
    debugFormat(...args);
    warnFormat(...args);
    errorFormat(...args);

}

interface enumloglevel {
    debug: number;
    info: number;
    warn: number;
    error: number;
    none: number;
}



declare var iwscore: IwsCore;
declare var log: Log;
declare var iwscommand: IwsCommand;
declare var enumloglevel: enumloglevel;
