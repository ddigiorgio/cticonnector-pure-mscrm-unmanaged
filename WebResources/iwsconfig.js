/**
 * Possible log levels:
 * 	enumloglevel.debug
 * 	enumloglevel.info
 * 	enumloglevel.warn
 * 	enumloglevel.error
 * 	enumloglevel.none 
 */
log.setLogLevel(enumloglevel.debug);

$(document).ready(function() {
										   
	var auth = {
		environment : "mypurecloud.com"
	};
	
	var params = {
		context: window,
		layoutType: "widget",
		integrationType: "pure-embeddable",
		url: "https://apps.mypurecloud.com/crm/softphoneMSDynamics/index.html?crm_domain=https://softphoneit.crm4.dynamics.com&sso=false",
		auth: auth
	}									   
										   
	iwscore.initCTI(params);

	iwscore.enableCTI();



});