
function networkError(message) {
    log.error(message);
}

function onConnectedSession(message) {
    //CheckAgentIntegrity(message);
}

function onDisconnectedSession(message) {
    alert("Interaction workspace has disconnected");
}

function onActivateSession(message) {

    switchInteraction();

}

function onDeactivateSession(message) {
}

function onEventAgentNotReady(message) {
}

function onEventAgentReady(message) {
}

function onEventAgentLogout(message) {
}

function onEventAgentLogin(message) {
}

//==================================================================
// Events MediaVoice
/*
* message.EVENT
* message.Place
* message.AgentID
* message.MediaType
* message.ConnectionID
* message.ANI
* message.DNIS
* message.CallType
* message.Duration
* message.TimeStamp
* message.attachdata
* message.EntrepriseLastInteractionEvent.PreviousConnID
*/
//==================================================================

function printAllAttachData(message) {
    log.debugFormat("called printAttachData on Event:[{0}]", message.EVENT);
    for (var key in message.attachdata) {
        log.debugFormat("attach key[{0}] value[{1}]", key, message.attachdata[key]);
    }
}

//RINGING
function onEventRingingInbound(message) {
    iwscore.openCTIPanel();
}

function onEventRingingInternal(message) {
}

function onEventRingingConsult(message) {
    iwscore.openCTIPanel();
}

//EventEstablished
function onEventEstablishedInbound(message) {
    log.info("onEventEstablishedInbound...");
    if (message.attachdata.CONTACT_ID) {
        log.debugFormat("Try to call InboundContactPopUpContactId with CONTACT_ID[{0}] ", message.attachdata.CONTACT_ID);
        InboundContactPopUpContactId(message);
    }
    else if (message.attachdata.ACCOUNT_ID) {
        log.debugFormat("Try to call InboundAccountPopUpAccountId with ACCOUNT_ID[{0}] ", message.attachdata.ACCOUNT_ID);
        InboundAccountPopUpAccountId(message);
    }
    else if (message.attachdata.ACTIVITY_ID) {
        log.debugFormat("Try to call PopUpPhoneCall with ACTIVITY_ID[{0}] ", message.ACTIVITY_ID);
        PopUpPhoneCall(message);
    }
    else {
        //message.ANI = "0825789423";
        log.debugFormat("Try to call InboundContactPopUpANI with ANI[{0}] ", message.ANI);
        InboundContactPopUpANI(message);
    }
}

function onEventEstablishedInternal(message) {
}

function onEventEstablishedConsult(message) {
}

function onEventEstablishedOutbound(message) {
    OutboundContactPopUpDNIS(message);
}

//EventHeld
function onEventHeldInbound(message) {
}

function onEventHeldInternal(message) {
}

function onEventHeldConsult(message) {
}

function onEventHeldOutbound(message) {
}

//EventRetrieved	
function onEventRetrievedInbound(message) {
}

function onEventRetrievedInternal(message) {
}

function onEventRetrievedConsult(message) {
}

function onEventRetrievedOutbound(message) {
}

//EventAttachedDataChanged
function onEventAttachedDataChangedInbound(message) {
}

function onEventAttachedDataChangedInternal(message) {
}

function onEventAttachedDataChangedConsult(message) {
}

function onEventAttachedDataChangedOutbound(message) {
}

//EventReleased
function onEventReleasedInbound(message) {
    //ReleaseCall(message);
    UpdateDuration(message);
}

function onEventReleasedInternal(message) {
}

function onEventReleasedConsult(message) {
}

function onEventReleasedOutbound(message) {
    //ReleaseCall(message);
    UpdateDuration(message);
}

//EventDialing
function onEventDialingInternal(message) {
}
function onEventDialingConsult(message) {
}
function onEventDialingOutbound(message) {
    iwscore.openCTIPanel();
}

//EventUserEvent
function onEventUserEvent(message) {
    //alert("onEventUserEvent");
    //printAllAttachData(message);		
}

//EventPartyChanged
function onEventPartyChangedInbound(message) {

    if (message.attachdata.CONTACT_ID) {
        log.debugFormat("Try to call InboundContactPopUpContactId with CONTACT_ID[{0}] ", message.attachdata.CONTACT_ID);
        InboundContactPopUpContactId(message);
    }
    else if (message.attachdata.ACCOUNT_ID) {
        log.debugFormat("Try to call InboundAccountPopUpAccountId with ACCOUNT_ID[{0}] ", message.attachdata.ACCOUNT_ID);
        InboundAccountPopUpAccountId(message);
    }
    else if (message.attachdata.ACTIVITY_ID) {
        log.debugFormat("Try to call PopUpPhoneCall with ACTIVITY_ID[{0}] ", message.ACTIVITY_ID);
        PopUpPhoneCall(message);
    }
    else {
        //message.ANI = "0825789423";
        log.debugFormat("Try to call InboundContactPopUpANI with ANI[{0}] ", message.ANI);
        InboundContactPopUpANI(message);
    }

}


//==================================================================
// Events MediaEmail
/*
* message.EVENT
* message.Place
* message.AgentID
* message.MediaType
* message.ConnectionID
* message.ANI
* message.DNIS
* message.CallType
* message.Duration
* message.TimeStamp
* message.attachdata
* message.EntrepriseLastInteractionEvent.PreviousConnID
*/
//==================================================================

function onEmailEventRingingInbound(message) {
    iwscore.openCTIPanel();
}

function onEmailEventEstablishedInbound(message) {
    log.info("onEmailEventEstablishedInbound...");
    emailInboundEstablishedScenario(message);
}

function onEmailEventEstablishedOutbound(message) {
    log.info("onEmailEventEstablishedOutbound...");
    if (message.attachdata) {
        if (message.attachdata.CONTACT_ID) {
            emailOutboundPopUpContactId(message);
        }
        else if (message.attachdata.ACCOUNT_ID) {
            emailOutboundPopUpAccountId(message);
        }
        else if (message.attachdata.ACTIVITY_ID) {
            PopUpTask(message);
        }
        else {
            emailOutboundContactPopUpEmailAddressAct(message);
        }
    } else {
        log.warn("onEmailEventEstablishedOutbound no attachdata available");
    }
}

function onEmailEventOpenedInbound(message) {
    /*PopUpContactAndActivity(message);
    UpdateEmailStatus(message, "In Progress");*/
    emailInboundEstablishedScenario(message);
}

function onEmailEventReplyOpenedOutbound(message) {
    /*PopUpContactAndActivity(message);
    UpdateEmailStatus(message, "In Progress");*/
    emailOutboundEstablishedScenario(message);
}

function onEmailEventReleasedInbound(message) {
    /*ReleaseEmail(message);*/
    log.info("onEmailEventReleasedInbound...");
    mediaReleaseInteraction(message);
}

function onEmailEventReleasedOutbound(message) {
    /*ReleaseEmail(message);*/
    log.info("onEmailEventReleasedOutbound...");
    mediaReleaseInteraction(message);
}

/**
* called when a Reply is sent
*/
function onEmailEventReplyReleased(message) {
    //med_EmailOutboundContactAccountPopUp(message);
    mediaReleaseInteraction(message);
}


function onEmailEventReplyEstablishedOutbound(message) {
    emailOutboundEstablishedScenario(message);
}


function onEmailEventReplyCancelled(message) {
    /*ReleaseEmail(message);*/
}

function emailInboundEstablishedScenario(message) {
    log.info("emailInboundEstablishedScenario...");
    if (message.attachdata) {
        if (message.attachdata.CONTACT_ID) {
            emailPopUpContactId(message);
        }
        else if (message.attachdata.ACCOUNT_ID) {
            emailPopUpAccountId(message);
        }
        else if (message.attachdata.ACTIVITY_ID) {
            PopUpTask(message);
        }
        else {
            emailContactPopUpEmailAddressAct(message);
        }
    } else {
        log.warn("emailInboundEstablishedScenario no attachdata available");
    }
}

function emailOutboundEstablishedScenario(message)
{
    log.info("emailOutboundEstablishedScenario...");
    if (message.attachdata) {
        message.attachdata.ACTIVITY_ID = "";
        if (message.attachdata.CONTACT_ID) {
            log.info("=============== CONTACT_ID ===================");
            emailOutboundPopUpContactId(message);
        }
        else if (message.attachdata.ACCOUNT_ID) {
            log.info("=============== ACCOUNT_ID ===================");
            emailOutboundPopUpAccountId(message);
        }
        else if (message.attachdata.ACTIVITY_ID) {
            log.info("=============== ACTIVITY_ID ===================");
            PopUpTask(message);
        }
        else {
            log.info("=============== emailOutboundContactPopUpEmailAddressAct ===================");
            emailOutboundContactPopUpEmailAddressAct(message);
        }
    } else {
        log.warn("emailOutboundEstablishedScenario no attachdata available");
    }
}

//==================================================================
//Events MediaWorkbin
//==================================================================

function onWorkbinTakenOut(message) {
    //alert("onWorkbinTakenOut message.MediaName: " + message.MediaName);	
}

function onWorkbinPlacedIn(message) {
    if (message.Interaction.InteractionMediatype == "email") {
        log.info("onWorkbinPlacedIn - email -> Execute UpdateEmailStatus with status Workbin");
        /*UpdateEmailStatus(message, "Workbin");*/
    }
}

//==================================================================
// Events MediaChat
/*
* message.EVENT
* message.Place
* message.AgentID
* message.MediaType
* message.ConnectionID
* message.ANI
* message.DNIS
* message.CallType
* message.Duration
* message.TimeStamp
* message.attachdata
* message.EntrepriseLastInteractionEvent.PreviousConnID
*/
//==================================================================

function onChatEventRingingInbound(message) {
    iwscore.openCTIPanel();
}

function onChatEventRingingConsult(message) {
}

function onChatEventEstablishedInbound(message) {
    log.info("onChatEventEstablishedInbound...");
    if (message.attachdata) {
        if (message.attachdata.CONTACT_ID) {
            chatPopUpContactId(message);
        }
        else if (message.attachdata.ACCOUNT_ID) {
            chatPopUpAccountId(message);
        }
        else if (message.attachdata.ACTIVITY_ID) {
            PopUpTask(message);
        }
        else {
            chatContactPopUpEmailAddressAct(message);
            //ChatAccountPopUpEmailAddressAct(message); // TO SCREEN POP ON ACCOUNT ENTITY
        }
    } else {
        log.warn("onChatEventEstablishedInbound no attachdata available");
    }
}

function onChatEventEstablishedConsult(message) {
}

function onChatEventReleasedInbound(message) {
    /*ReleaseChat(message);*/
    mediaReleaseInteraction(message);
}

function onChatEventReleasedConsult(message) {
}

function onChatEventTranscriptLink(message) {
    //Attach the chat transcript to the activity
    /*ChatTranscriptAttachment(message);*/
}

function onChatEventPartyRemovedInbound(message) {
}

function onChatEventPartyAddedInbound(message) {
}


function onChatEventPartyChangedInbound(message) {

    log.info("onChatEventPartyChangedInbound...");
    if (message.attachdata) {
        if (message.attachdata.CONTACT_ID) {
            chatPopUpContactId(message);
        }
        else if (message.attachdata.ACCOUNT_ID) {
            chatPopUpAccountId(message);
        }
        else if (message.attachdata.ACTIVITY_ID) {
            PopUpTask(message);
        }
        else {
            chatContactPopUpEmailAddressAct(message);
        }
    } else {
        log.warn("onChatEventPartyChangedInbound no attachdata available");
    }
}

//==================================================================
// WORKITEM Events 
//==================================================================

function onWorkitemEventRingingInbound(message) {
    //TODO SCREENPOPSR(message); 
}

function onWorkitemEventEstablishedInbound(message) {
    //TODO 
}

function onWorkitemEventOpenedInbound(message) {
    //TODO 
}

function onWorkitemEventReleasedInbound(message) {
    //TODO
    //log.info("onWorkitemEventReleasedInbound...");
}

//==================================================================
// SMS Events 
//==================================================================

function onSMSEventRingingInbound(message) {
    iwscore.openCTIPanel();
}

function onSMSEventEstablishedInbound(message) {
    /*if (message.attachdata.CONTACT_ID) {
        SMSInboundContactPopUpContactId(message);
    }
    else if (message.attachdata.ACCOUNT_ID) {
        //TODO	
    }
    else {
        SMSInboundContactPopUp(message);
    }*/
}

function onSMSEventReleasedInbound(message) {
    //SAME AS RELEASE CALL SO WE USE THE SAME FUNCTION CALL
    //ReleaseCall(message);
}

function onSMSEventEstablishedOutbound(message) {
   /* SMSOutboundContactPopUpDNIS(message);*/
}

function onSMSEventReleasedOutbound(message) {
   /* SMSOutboundContactAct(message);*/
}

function onSmsEventSendMessage(message) {
   /* SMSSendContactAct(message);*/
}

//==================================================================
//Events Facebook
//==================================================================
function onFacebookEventRingingInbound(message) {
    //log.info("onFacebookEventRingingInbound...");
    iwscore.openCTIPanel();
}

function onFacebookEventEstablishedInbound(message) {
    //log.info("onFacebookEventEstablishedInbound...");
    /*if (message.attachdata.CONTACT_ID) {
        FacebookInboundContactPopUpContactId(message);
    }
    else if (message.attachdata.ACTIVITY_ID) {
        FacebookInboundActivityPopUp(message);
    }
    else {
        FacebookInboundContactPopUp(message);
    }*/

}

function onFacebookEventReleasedInbound(message) {
    //log.info("onFacebookEventReleasedInbound...");
    //ReleaseFacebook(message);

}

function onFacebookEventReplyOutbound(message) {
    //log.info("onFacebookEventSessionInfo...");
    /*FacebookCommentAct(message);*/
}

function onFacebookEventSessionInfo(message) {
    //log.info("onFacebookEventSessionInfo...");
}

//==================================================================
//Events Twitter
//==================================================================

function onTwitterEventRingingInbound(message) {
    iwscore.openCTIPanel();
}

function onTwitterEventEstablishedInbound(message) {
    log.info("onTwitterEventEstablishedInbound...");
    /*if (message.attachdata.CONTACT_ID) {
        TwitterInboundContactPopUpContactId(message);
    }
    else if (message.attachdata.ACTIVITY_ID) {
        TwitterInboundActivityPopUp(message);
    }
    else {
        TwitterInboundContactPopUp(message);
    }*/
}

function onTwitterEventOpenedInbound(message) {
    log.info("onTwitterEventOpenedInbound...");
    /*if (message.attachdata.CONTACT_ID) {
        TwitterInboundContactPopUpContactId(message);
    }
    else if (message.attachdata.ACTIVITY_ID) {
        TwitterInboundActivityPopUp(message);
    }
    else {
        TwitterInboundContactPopUp(message);
    }*/

}

function onTwitterEventSessionInfo(message) {
    //log.info("onTwitterEventSessionInfo...");	
}

function onTwitterEventReleasedInbound(message) {
    //log.info("onTwitterEventReleasedInbound...");
    /*ReleaseTwitter(message);*/
}

function onTwitterEventReplyOutbound(message) {
    //log.info("onTwitterEventReply...");
    /*TwitterReplyContactAct(message);*/
}
function onTwitterEventRetweetOutbound(message) {
    //log.info("onTwitterEventRetweet...");
    /*TwitterRetweetContactAct(message);*/
}

function onTwitterEventDirectMessageOutbound(message) {
    //log.info("onTwitterEventDirectMessage...");
    /*TwitterDirectMessageContactAct(message);*/
}


//==================================================================
//Generic Events 
//==================================================================

function onSwitchInteraction(message) {
    //printAllAttachData(message);
    log.debugFormat("[onSwitchInteraction] parameters [{0}]", JSON.stringify(message));

    log.info("=================== onSwitchInteraction ===================");
    if (message.MediaType == "voice") {
        if (message.attachdata.CONTACT_ID) {
            log.debugFormat("Try to call InboundContactPopUpContactId with CONTACT_ID[{0}] ", message.attachdata.CONTACT_ID);
            InboundContactPopUpContactId(message);
        }
        else if (message.attachdata.ACCOUNT_ID) {
            log.debugFormat("Try to call InboundAccountPopUpAccountId with ACCOUNT_ID[{0}] ", message.attachdata.ACCOUNT_ID);
            InboundAccountPopUpAccountId(message);
        }
        else if (message.attachdata.ACTIVITY_ID) {
            log.debugFormat("Try to call PopUpPhoneCall with ACTIVITY_ID[{0}] ", message.ACTIVITY_ID);
            PopUpPhoneCall(message);
        }
        else {
            //message.ANI = "0825789423";
            log.debugFormat("Try to call InboundContactPopUpANI with ANI[{0}] ", message.ANI);
            InboundContactPopUpANI(message);
        }
    }
    else {
            if (message.attachdata.CONTACT_ID) {
                callSafetyFunction(message.MediaType + "PopUpContactId(message);", message);
            }
            else if (message.attachdata.ACCOUNT_ID) {
                callSafetyFunction(message.MediaType + "PopUpAccountId(message);", message);
            }
            else if (message.attachdata.ACTIVITY_ID) {
                PopUpTask(message);
            }
            else {
                callSafetyFunction(message.MediaType + "ContactPopUpEmailAddressAct(message);", message);
            }
    }
}
//alert("iwscriptwellformed");