function CheckAgentIntegrity(message) {
    top.SDK.REST.retrieveRecord(
           Xrm.Page.context.getUserId(),
           "SystemUser",
           "DomainName", null,
           function (systemUser) {
               
               var domainName = systemUser.DomainName.split("\\");
               var loginName = domainName[1];
              
               /*if (loginName != message.UserName) {
                   alert("Microsoft CRM UserName is not the same of the iWS UserName, Logout and Login again! 2");
                   window.close();
               }*/
           },
           errorHandler
       );
}

/**
    
*/
function InboundContactPopUpContactId(message) {

    log.info("Executing InboundContactPopUpContactId...");

    var description = "Call Inbound  from " + message.ANI + " - ConnectionId: " + message.ConnectionID;

    var parameters = {};
    parameters.EntityNameToOpen = "contact";
    parameters.EntityIdToOpen = message.attachdata.CONTACT_ID;
    parameters.task = {
        Title: message.ConnectionID, InteractionId: message.ConnectionID, MediaType: 1, Description: description, Priority: 1, Outgoing: false,
        LogicalName: "phonecall", ReferenceObjectId: message.attachdata.CONTACT_ID, ReferenceObjectLogicalName: parameters.EntityNameToOpen,
        ReferenceObjectParticipationTypeValue: 1, PhoneNumber: message.ANI
    };
    parameters.contact = { Id: message.attachdata.CONTACT_ID };
    parameters.connection = { Id: message.InteractionID };

    if (message.attachdata.ACTIVITY_ID) {
        log.debugFormat("InboundContactPopUpContactId with CONTACT_ID[{0}] and ACTIVITY_ID[{1}]", message.attachdata.CONTACT_ID, message.attachdata.ACTIVITY_ID);
        openEntity(parameters);
    }
    else {
        log.debugFormat("InboundContactPopUpContactId with CONTACT_ID[{0}]", message.attachdata.CONTACT_ID);
        CreatePhoneCall(parameters).pipe(CreateActivityParty).pipe(CreateActivityPartyForAgent).pipe(attachIWSData).pipe(openEntity);
    }
}

/**
    
*/
function InboundAccountPopUpAccountId(message) {

        log.info("Executing InboundAccountPopUpAccountId...");

        var description = "Call Inbound  from " + message.ANI + " - ConnectionId: " + message.ConnectionID;

        var parameters = {};
        parameters.EntityNameToOpen = "account";
        parameters.EntityIdToOpen = message.attachdata.ACCOUNT_ID;
        parameters.task = {
            Title: message.ConnectionID, InteractionId: message.ConnectionID, MediaType: 1, Description: description, Priority: 1, Outgoing: false,
            LogicalName: "phonecall", ReferenceObjectId: message.attachdata.ACCOUNT_ID, ReferenceObjectLogicalName: parameters.EntityNameToOpen,
            ReferenceObjectParticipationTypeValue: 1, PhoneNumber: message.ANI
        };
        parameters.account = { Id: message.attachdata.ACCOUNT_ID };
        parameters.connection = { Id: message.InteractionID };

        if (message.attachdata.ACTIVITY_ID) {
            log.debugFormat("InboundAccountPopUpAccountId with ACCOUNT_ID[{0}] and ACTIVITY_ID[{1}]", message.attachdata.ACCOUNT_ID, message.attachdata.ACTIVITY_ID);
            openEntity(parameters);
        }
        else {
            log.debugFormat("InboundAccountPopUpAccountId with ACCOUNT_ID[{0}]", message.attachdata.ACCOUNT_ID);
            CreatePhoneCall(parameters).pipe(CreateActivityParty).pipe(CreateActivityPartyForAgent).pipe(attachIWSData).pipe(openEntity);
        }

}

/**
    
*/
function InboundContactPopUpANI(message) {
    log.info("Executing InboundContactPopUpANI...");
    var description = "Call Inbound  from " + message.ANI + " - ConnectionId: " + message.ConnectionID;

    var parameters = {};
    parameters.EntityNameToOpen = "contact";
    parameters.task = {
        Title: message.ConnectionID, InteractionId: message.ConnectionID, MediaType: 1, Description: description, Priority: 1, Outgoing: false,
        LogicalName: "phonecall", ReferenceObjectLogicalName: parameters.EntityNameToOpen,
        ReferenceObjectParticipationTypeValue: 1, PhoneNumber: message.ANI
    };
    parameters.contact = { BusinessPhone: message.ANI };
    parameters.connection = { Id: message.InteractionID };

    CreatePhoneCall(parameters).pipe(retrieveContactByBusinessPhone).pipe(CreateActivityParty).pipe(CreateActivityPartyForAgent).pipe(attachIWSData).pipe(openEntity);
    //with wizard
    //CreatePhoneCall(parameters).pipe(retrieveContactByBusinessPhoneWithWizard).pipe(CreateActivityParty).pipe(CreateActivityPartyForAgent).pipe(attachIWSData).pipe(openEntity).pipe(OpenWizard);
}

/**
    
*/
function OutboundContactPopUpDNIS(message) {

    log.info("Executing OutboundContactPopUpDNIS...");

    var description = "Call Oubound to " + message.DNIS + " - ConnectionId: " + message.ConnectionID;

    var parameters = {};
    parameters.EntityNameToOpen = "contact";
    parameters.task = {
        Title: message.ConnectionID, InteractionId: message.ConnectionID, MediaType: 1, Description: description, Priority: 1, Outgoing: true,
        LogicalName: "phonecall", ReferenceObjectLogicalName: parameters.EntityNameToOpen,
        ReferenceObjectParticipationTypeValue: 2, PhoneNumber: message.DNIS
    };
    parameters.contact = { BusinessPhone: message.DNIS };
    parameters.connection = { Id: message.InteractionID };

    CreatePhoneCall(parameters).pipe(retrieveFirstContactByBusinessPhone).pipe(CreateActivityParty).pipe(CreateActivityPartyForAgent).pipe(attachIWSData).pipe(openEntity);
}

function PopUpTask(message) {
    var parameters = {};
    parameters.EntityNameToOpen = "task";
    parameters.EntityIdToOpen = message.attachdata.ACTIVITY_ID;
    parameters.connection = { Id: message.InteractionID };
    openEntity(parameters);
}

function PopUpPhoneCall(message) {
    var parameters = {};
    parameters.EntityNameToOpen = "phonecall";
    parameters.EntityIdToOpen = message.attachdata.ACTIVITY_ID;
    parameters.connection = { Id: message.InteractionID };
    openEntity(parameters);
}


function openActivity(parameters) {
    if (parameters) {
        parameters.EntityNameToOpen = "task";
        parameters.EntityIdToOpen = parameters.task.Id;
	    openEntity(parameters);
    }   
}

function openContact(parameters) {
    if (parameters) {
        parameters.EntityNameToOpen = "contact";
        parameters.EntityIdToOpen = parameters.contact.Id;
    	openEntity(parameters);
    }
}

function openEntity(parameters) {
    var def = $.Deferred();
    if (!isAppMode()) {
        if (parameters) {
            var currid = getSelectedInteractionId();

            var curmessage = getSelectedInteraction();
            log.debugFormat("openEntity current message [{0}]", curmessage);
            if (isCampaign(curmessage)) {
                log.debugFormat("openEntity current ID [{0}] is a campaign", currid);
                if (curmessage.ConnectionID) {
                    currid = curmessage.ConnectionID;
                }
            }

            log.debugFormat("openEntity current ID [{0}] connectionID[{1}]", currid);

            //if (parameters.connection.Id == currid) {
            if (parameters.connection.Id.toLowerCase() == currid.toLowerCase()) {
                log.debugFormat("[openEntity] parameters [{0}]", JSON.stringify(parameters));
                Xrm.Utility.openEntityForm(parameters.EntityNameToOpen, parameters.EntityIdToOpen);
            }
            else {
                log.debug("No openEntity");
            }
        }
    } else {
		log.debugFormat("[openEntity] parameters [{0}]", JSON.stringify(parameters));
        Xrm.Utility.openEntityForm(parameters.EntityNameToOpen, parameters.EntityIdToOpen);
    }
    def.resolve(parameters);
    return def;
}

function retrieveContactByBusinessPhone(parameters) {
    var def = $.Deferred();
    log.debugFormat("[retrieveContactByBusinessPhone] parameters [{0}]", JSON.stringify(parameters));
    log.debug("retrieveContactByBusinessPhone...");
    var options = "$select=ContactId,Telephone1&$filter=Telephone1 eq '" + encodeURI(parameters.contact.BusinessPhone) + "'";
    log.debug("retrieveContactByBusinessPhone options: " + options);

    SDK.REST.retrieveMultipleRecords("Contact",
        options,
        function (contacts) {
            if ((contacts) && (contacts.length == 1)) {
                log.debug("retrieveContactByBusinessPhone contacts length = 1");
                log.debug("retrieveContactByBusinessPhone contact: " + contacts[0].ContactId);
                parameters.contact.Id = "" + contacts[0].ContactId;
                parameters.EntityIdToOpen = parameters.contact.Id;
                parameters.task.ReferenceObjectId = parameters.contact.Id;
                def.resolve(parameters);
            } else {
                log.debug("retrieveContactByBusinessPhone length != 1");
                parameters.EntityNameToOpen = "phonecall";
                parameters.EntityIdToOpen = parameters.task.Id;
                def.resolve(parameters);
                /*
                    Wizard implementation
                */
                //SetFiledParameterOnPhoneCallWithBusinessPhone(parameters).pipe(OpenContactFindingDialogProcessForPhoneCall);
                //SetFiledParameterOnPhoneCallWithBusinessPhone(parameters).pipe(OpenContactFindingDialogProcessForTask);
            }
        },
        errorHandler,
        function () { });

    return def;
}

function retrieveContactByBusinessPhoneWithWizard(parameters) {
    var def = $.Deferred();
    log.debugFormat("[retrieveContactByBusinessPhone] parameters [{0}]", JSON.stringify(parameters));
    log.debug("retrieveContactByBusinessPhone...");
    var options = "$select=ContactId,Telephone1&$filter=Telephone1 eq '" + encodeURI(parameters.contact.BusinessPhone) + "'";
    log.debug("retrieveContactByBusinessPhone options: " + options);

    SDK.REST.retrieveMultipleRecords("Contact",
        options,
        function (contacts) {
            if ((contacts) && (contacts.length == 1)) {
                log.debug("retrieveContactByBusinessPhone contacts length = 1");
                log.debug("retrieveContactByBusinessPhone contact: " + contacts[0].ContactId);
                parameters.contact.Id = "" + contacts[0].ContactId;
                parameters.EntityIdToOpen = parameters.contact.Id;
                parameters.task.ReferenceObjectId = parameters.contact.Id;
                def.resolve(parameters);
            } else {
                log.debug("retrieveContactByBusinessPhone length != 1");
                parameters.EntityNameToOpen = "phonecall";
                parameters.EntityIdToOpen = parameters.task.Id;

                parameters.Wizard = {};
                parameters.Wizard.Id = "981CC42C-1611-49AC-BB79-CDA06EB6FF0F";
                parameters.Wizard.EntityName = "phonecall";
                parameters.Wizard.EntityId = parameters.task.Id;
                parameters.Wizard.CancelCallBack = null;
                parameters.Wizard.FinishCallBack = OnContactFindingPhoneCallDialogProcessClosed.bind(parameters);
                def.resolve(parameters);
                /*
                    Wizard implementation
                */
                //SetFiledParameterOnPhoneCallWithBusinessPhone(parameters).pipe(OpenContactFindingDialogProcessForPhoneCall);
                //SetFiledParameterOnPhoneCallWithBusinessPhone(parameters).pipe(OpenContactFindingDialogProcessForTask);
            }
        },
        errorHandler,
        function () { });

    return def;
}

function retrieveFirstContactByBusinessPhone(parameters) {
    log.info("retrieveFirstContactByBusinessPhone...");
    var def = $.Deferred();
    var options = "$select=ContactId,Telephone1&$top=1&$filter=Telephone1 eq '" + parameters.contact.BusinessPhone + "'";

    SDK.REST.retrieveMultipleRecords("Contact",
        options,
        function (contacts) {
            if (contacts && contacts.length == 1) {
                parameters.contact.Id = "" + contacts[0].ContactId;
                parameters.EntityIdToOpen = parameters.contact.Id;
                parameters.task.ReferenceObjectId = parameters.contact.Id;
                def.resolve(parameters);
            } else {
                log.debugFormat("Unable to find contact with business phone:[{0}]", parameters.contact.BusinessPhone);
            }
        },
        errorHandler,
        function () { });

    return def;
}

/**
    set the BusinessPhone on softphone_parameter of a PhoneCall Form
    the parameter is read from Dialog Process to select a Contact
*/
function SetFiledParameterOnPhoneCallWithBusinessPhone(parameters) {
    log.info("SetFiledParameterOnPhoneCallWithBusinessPhone...");
    var def = $.Deferred();
    var task = {};
    task.softphon_parameter = parameters.contact.BusinessPhone;
    SDK.REST.updateRecord(parameters.task.Id,
                         task,
                         "PhoneCall",
                         function () {
                             def.resolve(parameters);
                         },
                         errorHandler
                       );
    return def;
}

/**
    Update all fields available in the "parameters.UpdateFields" object
    on a PhoneCall Entity by ID
*/
function UpdatePhoneCall(parameters) {
    log.info("UpdatePhoneCall...");
    var def = $.Deferred();
    var task = parameters.UpdateFields;
    //var task = {};
    SDK.REST.updateRecord(parameters.task.Id,
                         task,
                         "PhoneCall",
                         function () {
                             def.resolve(parameters);
                         },
                         errorHandler
                       );
    return def;
}

function UpdateTask(parameters) {
    log.info("UpdateTask...");
    var def = $.Deferred();
    var task = parameters.UpdateFields;
    //var task = {};
    SDK.REST.updateRecord(parameters.task.Id,
                         task,
                         "Task",
                         function () {
                             def.resolve(parameters);
                         },
                         errorHandler
                       );
    return def;
}

function chatContactPopUpEmailAddressAct(message) {
    log.info("chatContactPopUpEmailAddressAct...");
    var description = "Chat Inbound - ConnectionId: " + message.ConnectionID;
    mediaContactPopUpEmailAddressAct(3, description, message);
}

function chatPopUpContactId(message) {
    var description = "Chat " + message.InteractionID + " : Subject: " + message.attachdata.Subject;
    mediaPopUpContactId(3, description, message);
}

function chatPopUpAccountId(message) {
    var description = "Chat " + message.InteractionID + " : Subject: " + message.attachdata.Subject;
    mediaPopUpAccountId(3, description, message);
}


function emailContactPopUpEmailAddressAct(message) {
    log.info("emailContactPopUpEmailAddressAct...");
    var description = "Email Inbound - ConnectionId: " + message.ConnectionID;
    mediaContactPopUpEmailAddressAct(2, description, message);
}

function emailOutboundContactPopUpEmailAddressAct(message) {
    log.info("emailOutboundContactPopUpEmailAddressAct...");
    var description = "Email Outbound - ConnectionId: " + message.ConnectionID;
    mediaContactPopUpEmailAddressAct(2, description, message);
}

function emailPopUpContactId(message) {
    var description = "Email " + message.InteractionID + " : Subject: " + message.attachdata.Subject;
    mediaPopUpContactId(2, description, message);
}

function emailPopUpAccountId(message) {
    var description = "Email " + message.InteractionID + " : Subject: " + message.attachdata.Subject;
    mediaPopUpAccountId(2, description, message);
}

function emailOutboundPopUpContactId(message) {
    var description = "Email Outbound " + message.InteractionID + " : Subject: " + message.attachdata.Subject;
    mediaPopUpContactId(2, description, message);
}

function emailOutboundPopUpAccountId(message) {
    var description = "Email Outbound " + message.InteractionID + " : Subject: " + message.attachdata.Subject;
    mediaPopUpAccountId(2, description, message);
}

function EmailInboundContactPopUpContactId(message) {

    log.info("Executing EmailInboundContactPopUpContactId...");

    var body = "Email Inbound from Contact [" + message.attachdata.CONTACT_ID + "] - ConnectionId: " + message.ConnectionID;

    var parameters = {};
    parameters.EntityNameToOpen = "contact";
    parameters.EntityIdToOpen = message.attachdata.CONTACT_ID;
    parameters.task = {
        Title: message.ConnectionID, Description: body,
        LogicalName: "email", ReferenceObjectId: message.attachdata.CONTACT_ID, ReferenceObjectLogicalName: parameters.EntityNameToOpen,
        ReferenceObjectParticipationTypeValue: 1
    };
    parameters.contact = { Id: message.attachdata.CONTACT_ID };
    parameters.connection = { Id: message.InteractionID };

    if (message.attachdata.ACTIVITY_ID) {
        log.debugFormat("EmailInboundContactPopUpContactId with CONTACT_ID[{0}] and ACTIVITY_ID[{1}]", message.attachdata.CONTACT_ID, message.attachdata.ACTIVITY_ID);
        openEntity(parameters);
    }
    else {
        log.debugFormat("EmailInboundContactPopUpContactId with CONTACT_ID[{0}]", message.attachdata.CONTACT_ID);
        CreateEmail(parameters).pipe(CreateActivityParty).pipe(CreateActivityPartyForAgent).pipe(attachIWSData).pipe(openEntity);
    }
}

/**
    Popup on "Contact View" for the Contact with id = CONTACT_ID 
    the function is used for each media (chat, email, sms etc...)
*/
function mediaPopUpContactId(mediatype, description, message) {

    log.info("Executing mediaContactPopUpContactId...");

    var parameters = {};
    parameters.EntityNameToOpen = "contact";
    parameters.EntityIdToOpen = message.attachdata.CONTACT_ID;
    parameters.task = { Title: message.ConnectionID, InteractionId: message.InteractionId, MediaType: mediatype, Description: description, Priority: 1 };
    parameters.contact = { Id: message.attachdata.CONTACT_ID };
    parameters.connection = { Id: message.InteractionID };

    if (message.attachdata.ACTIVITY_ID) {
        log.debugFormat("MediaContactPopUpContactId with CONTACT_ID[{0}] and ACTIVITY_ID[{1}]", message.attachdata.CONTACT_ID, message.attachdata.ACTIVITY_ID);
        openEntity(parameters);
    }
    else {
        log.debugFormat("MediaContactPopUpContactId with CONTACT_ID[{0}]", message.attachdata.CONTACT_ID);
        CreateNewTask(parameters).pipe(associateContactToTask).pipe(attachIWSData).pipe(openEntity);
    }
}

/**
    Popup on "Account View" for the Account with id = ACCOUNT_ID 
    the function is used for each media (chat, email, sms etc...)
*/
function mediaPopUpAccountId(mediatype, description, message) {

    log.info("Executing MediaPopUpAccountId...");

    var parameters = {};
    parameters.EntityNameToOpen = "account";
    parameters.EntityIdToOpen = message.attachdata.ACCOUNT_ID;
    parameters.task = { Title: message.ConnectionID, InteractionId: message.ConnectionID, MediaType: mediatype, Description: description, Priority: 1 };
    parameters.contact = { Id: message.attachdata.ACCOUNT_ID };
    parameters.connection = { Id: message.InteractionID };

    CreateNewTask(parameters).pipe(associateAccountToTask).pipe(attachIWSData).pipe(openEntity);
}

function mediaContactPopUpEmailAddressAct(mediatype, description, message) {
    log.info("mediaContactPopUpEmailAddressAct...");
    var parameters = {};

    parameters.task = { Title: message.ConnectionID, InteractionId: message.ConnectionID, MediaType: mediatype, Description: description, Priority: 1 };
    parameters.contact = { Email: message.attachdata.EmailAddress };
    //parameters.contact = { Email: "digiorgio@softphone.it" };
    parameters.connection = { Id: message.InteractionID };

    CreateNewTask(parameters).pipe(retrieveContactByEmail).done(associateContactToTask, attachIWSData, openContact).fail(attachIWSData, openActivity);
    //pipe(associateContactToTask).pipe(attachIWSData).pipe(openContact);
}

//==================== Get Record ===================

/**
    Read the 
*/
function GetContactByTaskId(parameters) {
    log.info("GetContactByTaskId...");
    var def = $.Deferred();
    SDK.REST.retrieveRecord(
				 parameters.task.Id,
				 "Task",
				 "RegardingObjectId", null,
				 function (task) {
				     if (task.RegardingObjectId && task.RegardingObjectId.Id) {
				         parameters.contact.Id = "" + task.RegardingObjectId.Id;
				         parameters.task.ReferenceObjectId = parameters.contact.Id;
				         def.resolve(parameters);
				     }
				 },
				 errorHandler
			   );
    return def;
}

function GetContactByPhoneCallId(parameters) {
    log.info("GetContactByPhoneCallId...");
    var def = $.Deferred();
    SDK.REST.retrieveRecord(
				 parameters.task.Id,
				 "PhoneCall",
				 "RegardingObjectId", null,
				 function (task) {
				     if (task.RegardingObjectId && task.RegardingObjectId.Id) {
				         parameters.contact.Id = "" + task.RegardingObjectId.Id;
				         parameters.task.ReferenceObjectId = parameters.contact.Id;
				         def.resolve(parameters);
				     }
				     else {
				         log.info("GetContactByPhoneCallId no contact selected...");
				         parameters.EntityNameToOpen = "phonecall";
				         parameters.EntityIdToOpen = parameters.task.Id;
				         def.reject(parameters);
				     }
				 },
				 errorHandler
			   );
    return def;
}

//============================================================
// Wizard section
//============================================================
function OpenWizard(parameters) {
    top.log.debug("OpenWizard starting");
    if (!parameters.Wizard)
        return;

    var width = 600;
    if (parameters.Wizard.Width)
        width = parameters.Wizard.Width;

    var height = 400;
    if (parameters.Wizard.Height)
        height = parameters.Wizard.Height;

    Process.callDialog(parameters.Wizard.Id, parameters.Wizard.EntityName,
			parameters.Wizard.EntityId,
			width,
			height,
			parameters.Wizard.CancelCallBack,
			parameters.Wizard.FinishCallBack
		);
}

function OnContactFindingDialogProcessClosed(parameters) {
    GetContactByTaskId(parameters).pipe(attachIWSData).pipe(openContact);
}

function OnContactFindingPhoneCallDialogProcessClosed(parameters) {
    GetContactByPhoneCallId(parameters).done(CreateActivityParty, CreateActivityPartyForAgent, attachIWSData, openContact).fail(CreateActivityPartyForAgent, openEntity);
}
function OnContactFindingPhoneCallDialogProcessClosed(parameters) {
    if (!parameters)
        parameters = this;

    top.log.debug("OnContactFindingPhoneCallDialogProcessClosed called... : " + parameters);
    parameters.Wizard = null;
    //GetContactByPhoneCallId(parameters).done(CreateActivityParty, CreateActivityPartyForAgent, attachIWSData, openContact).fail(CreateActivityPartyForAgent, openEntity);
    GetContactByPhoneCallId(parameters).done(attachIWSData, openContact).fail(openEntity);
}

//============================================================

function retrieveContactByEmail(parameters) {
    log.info("retrieveContactByEmail...");
    var def = $.Deferred();
    var options = "$select=ContactId,EMailAddress1&$top=1&$filter=EMailAddress1 eq '" + parameters.contact.Email + "'";
    log.debug("options: " +  options);
    SDK.REST.retrieveMultipleRecords("Contact",
        options,
        function (contact) {
            if (contact && contact.length == 1) {
                log.debugFormat("retrieveContactByEmail found contact with id [{0}]", contact[0].ContactId);
                parameters.contact.Id = "" + contact[0].ContactId;
                def.resolve(parameters);
            } else {
                log.debugFormat("retrieveContactByEmail not found contact with email [{0}]", parameters.contact.Email);
                def.reject(parameters);
            }
        },
        errorHandler,
        function () { }
    );
       
    return def;
}

function ReleaseCall(message) {
    
    log.info("Executing ReleaseCall...");
    //ActualDurationMinutes
    var parameters = {};
    parameters.task = {
        Id: message.attachdata.ACTIVITY_ID, LogicalName: "phonecall",
        State: 1, Status: 2
    };

    var duration = 1;
    if(message.Duration)
        duration = Math.ceil(message.Duration / 60);
    log.debugFormat("ReleaseCall try to update duration [{0}]", duration);
    parameters.UpdateFields = { ActualDurationMinutes: duration };

    UpdatePhoneCall(parameters).pipe(setEntityState);

}

function UpdateDuration(message) {

    log.info("Executing UpdateDuration...");
    //ActualDurationMinutes
    var parameters = {};
    parameters.task = {
        Id: message.attachdata.ACTIVITY_ID, LogicalName: "phonecall"
    };

    var duration = 1;
    if (message.Duration)
        duration = Math.ceil(message.Duration / 60);
    log.debugFormat("ReleaseCall try to update duration [{0}]", duration);
    parameters.UpdateFields = { ActualDurationMinutes: duration };

    UpdatePhoneCall(parameters);

}

function mediaReleaseInteraction(message) {

    log.info("Executing mediaReleaseInteraction...");
    //ActualDurationMinutes
    var parameters = {};
    parameters.task = {
        Id: message.attachdata.ACTIVITY_ID, LogicalName: "task",
        State: 1, Status: 5
    };

    if (message.Duration) {
        var duration = 1;
        duration = Math.ceil(message.Duration / 60);
        log.debugFormat("mediaReleaseInteraction try to update duration [{0}]", duration);
        parameters.UpdateFields = { ActualDurationMinutes: duration };
        UpdateTask(parameters).pipe(setEntityState);
    }
    else {
        setEntityState(parameters);
    }

}

function associateContactToTask(parameters) {
    var def = $.Deferred();
    SDK.REST.associateRecords(parameters.contact.Id,
           "Contact",
           "Contact_Tasks",
           parameters.task.Id,
           "Task",
           function () {
               //openObj(2, parameters.contact.Id);
               def.resolve(parameters);
           },
           errorHandler
         );

    return def;
}

function associateAccountToTask(parameters) {
    var def = $.Deferred();

    SDK.REST.associateRecords(parameters.contact.Id,
           "Account",
           "Account_Tasks",
           parameters.task.Id,
           "Task",
           function () {
               //openObj(1, parameters.account.Id);
               def.resolve(parameters);
           },
           errorHandler
         );

    return def;
}

function attachIWSData(parameters) {
    var def = $.Deferred();

    var myCollection = createUserData();
    if (parameters.task && parameters.task.Id) {
        myCollection.put("ACTIVITY_ID", parameters.task.Id);
    }
    if (parameters.contact && parameters.contact.Id)
        myCollection.put("CONTACT_ID", parameters.contact.Id);
    if (parameters.account && parameters.account.Id)
        myCollection.put("ACCOUNT_ID", parameters.account.Id);

    if (!myCollection.isEmpty())
        SetAttachdataById(parameters.connection.Id, myCollection);

    def.resolve(parameters);
    return def;
}

//====================== Update =====================
function setEntityState(parameters) {
    var def = $.Deferred();
    log.info("setEntityState");
    var entityReference = new Sdk.EntityReference(parameters.task.LogicalName, parameters.task.Id);
    var request = new Sdk.SetStateRequest(entityReference, parameters.task.State, parameters.task.Status);

    Sdk.Async.execute(request,
                       function () {
                           def.resolve(parameters);
                       },
                       errorHandler);
    return def;
}

//================= Creation ================
function CreateNewTask(parameters) {
    log.info("CreateNewTask...");
    var def = $.Deferred();

    var task = {
        Subject: parameters.task.Title, Description: parameters.task.Description, PriorityCode: { Value: parameters.task.PriorityCode }, softphon_iwsinteractionid: parameters.task.InteractionId,
        softphon_IWSMediaName: { Value: parameters.task.MediaType }
    };
    SDK.REST.createRecord(
     task,
     "Task",
     function (task) {
         parameters.task.Id = task.ActivityId;
         def.resolve(parameters);
     },
     errorHandler
   );

    return def;
}

function CreatePhoneCall(parameters) {
    //PhoneNumber: parameters.task.PhoneNumber,  
    var def = $.Deferred();
    log.debug("CreatePhoneCall...");
    var task = {
        Subject: parameters.task.Title, Description: parameters.task.Description, PhoneNumber: parameters.task.PhoneNumber,
        PriorityCode: { Value: parameters.task.PriorityCode }, DirectionCode: parameters.task.Outgoing,
        softphon_IWSInteractionId: parameters.task.InteractionId, softphon_IWSMediaName: { Value: parameters.task.MediaType }
    };
    log.debug("CreatePhoneCall try to createRecord");
    SDK.REST.createRecord(
			task,
			"PhoneCall",
			function (task) {
			    parameters.task.Id = task.ActivityId;
			    def.resolve(parameters);
			},
            errorHandler
    );

    return def;
}

function CreateEmail(parameters) {

    log.debug("CreateEmailCall...");

    var def = $.Deferred();

    var task = {
        Subject: parameters.task.Title, Description: parameters.task.Description
    };
    log.debug("CreateEmailCall try to createRecord");
    SDK.REST.createRecord(
			task,
			"Email",
			function (task) {
			    parameters.task.Id = task.ActivityId;
			    def.resolve(parameters);
			},
            errorHandler
    );

    return def;
}

function CreateActivityParty(parameters) {
    var def = $.Deferred();

    if (!parameters.task.ReferenceObjectId)
    {
        log.debug("[CreateActivityParty] ReferenceObjectId not available!");
        def.resolve(parameters);
        return def;
    }

    log.debug("CreateActivityParty...");
    var activityParty = {};

    activityParty.ActivityId = {
        Id: parameters.task.Id,
        LogicalName: parameters.task.LogicalName
    };

    activityParty.PartyId = {
        Id: parameters.task.ReferenceObjectId,
        LogicalName: parameters.task.ReferenceObjectLogicalName
    };

    activityParty.ParticipationTypeMask = { Value: parameters.task.ReferenceObjectParticipationTypeValue };

    SDK.REST.createRecord(
                     activityParty,
                     "ActivityParty",
                     function (task) {
                         def.resolve(parameters);
                     },
                     errorHandler

     );
    return def;
}

function CreateActivityPartyForAgent(parameters) {
    var def = $.Deferred();
    log.debug("CreateActivityPartyForAgent...");
    var activityParty = {};

    activityParty.ActivityId = {
        Id: parameters.task.Id,
        LogicalName: parameters.task.LogicalName
    };

    activityParty.PartyId = {
        Id: Xrm.Page.context.getUserId(),
        LogicalName: "systemuser"
    };

    if (parameters.task.ReferenceObjectParticipationTypeValue == 1)
        activityParty.ParticipationTypeMask = { Value: 2 };
    else
        activityParty.ParticipationTypeMask = { Value: 1 };

    SDK.REST.createRecord(
                     activityParty,
                     "ActivityParty",
                     function (task) {
                         def.resolve(parameters);
                     },
                     errorHandler
     );
    return def;
}

function errorHandler(error) {
    alert(error.message);
}


/*function MakeCallFromGrid(recordId) {
    MakeCall(recordId[0]);
}

function MakeCallFromForm() {
    MakeCall(Xrm.Page.data.entity.getId());
}

function MakeCall(contactID) {
   
    top.SDK.REST.retrieveRecord(
        contactID,
        "Contact",
        "Address1_Telephone1", null,
        function (contact) {
            if (contact.Address1_Telephone1)
            {
                alert(contact.Address1_Telephone1);
                //makeacall("'"+contact.Address1_Telephone1+"'");
            }
            else {
                alert("Any phone number found for selected contact!");
            }
        },
        errorHandler
    );
    
}*/

function OutboundCampaing(message) {
    log.info("Executing OutboundCampaing...");
    var campaign = message.attachdata.GSW_CAMPAIGN_NAME;
    var campaignPhone = message.attachdata.GSW_PHONE;
    log.info("campaign = " + campaign);
    log.info("GSW_PHONE = " + campaignPhone);

    if (campaign != "") {
        log.info("OutboundCampaing Configured...");
        var description = "Call Oubound to " + campaignPhone;
        var parameters = {};
        parameters.EntityNameToOpen = "contact";
        parameters.task = {
            PhoneNumber: campaignPhone
        };
        parameters.contact = { BusinessPhone: message.attachdata.GSW_PHONE };
        //parameters.connection = { Id: message.InteractionID };
        //retrieveFirstContactByBusinessPhone(parameters).pipe(openEntityPreview);
        retrieveContactByBusinessPhone(parameters).pipe(openEntityPreview);
    }
    else {
        log.info("No OutboundCampaing Configured...");
    }
}

function openEntityPreview(parameters) {
    var def = $.Deferred();
    log.debugFormat("[openEntity] parameters [{0}]", JSON.stringify(parameters));
    if (parameters.EntityIdToOpen && parameters.EntityNameToOpen) {
        Xrm.Utility.openEntityForm(parameters.EntityNameToOpen, parameters.EntityIdToOpen);
    } else {
        top.goTo("SFA", "Customers", "nav_conts");
    }
    def.resolve(parameters);
    return def;
}
