var deleteFolderRecursive = function(path) {
  if( fs.existsSync(path) ) {
    fs.readdirSync(path).forEach(function(file,index){
      var curPath = path + "/" + file;
      if(fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
};

var copyFolder = function(_path, destinationFolder, prefixFile) {
  if (!fs.existsSync(destinationFolder)){
    fs.mkdirSync(destinationFolder);
  }

  if( fs.existsSync(_path) ) {
    fs.readdirSync(_path).forEach(function(file,index){
      var curPath = path.join(_path ,file);   
      console.log("curPath: " + curPath);
      copyAndRenameFile(curPath, prefixFile + file, _path, destinationFolder);
    });   
  }
};

var copyAndRenameFile = function(filename, crmFileName, dirsource, dirdest) {
  console.log("filename: " + filename);
  console.log("crmFileName: " + crmFileName);
  try {
    var filesource = fs.readFileSync(`${filename}`, 'utf8');
    var stream = fs.createWriteStream(`${dirdest}/${crmFileName}`);
    stream.once('open', function (fd) {
      stream.write(filesource);
      stream.end();
      console.log("file copied");
    });
  } catch (e) {
    console.log("Error in copyFile: " + e.message);
  }
};

function copyAndRenameRawFile(filename, crmFileName, dirsource, dirdest)
{
	fs.readFile(filename, function (err, data) {
		if (err) throw err;
		fs.writeFile(dirdest + '/' + crmFileName, data, function (err) {
			if (err) throw err;
			console.log('File saved!' + filename);
		});
	});
}


var replaceStringInFile = function(filename, pre, post) {
  fs.readFile(filename, 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    var result = data.replace(pre, post);
  
    fs.writeFile(filename, result, 'utf8', function (err) {
       if (err) return console.log(err);
    });
  });
};

var pkg = require("../package.json");
var path = require('path');
var fs = require('fs');

var version = pkg.version;
console.log("Package Version: " + version);
var distFolder = path.join(__dirname ,`../dist/deploy/`);
var webResourceFolder = path.join(__dirname ,`../WebResources/`);

console.log(webResourceFolder);

deleteFolderRecursive(distFolder);
if (!fs.existsSync(distFolder)){
  fs.mkdirSync(distFolder);
}
copyFolder(webResourceFolder, distFolder, "softphon_");